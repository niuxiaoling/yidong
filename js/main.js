window.onload=function () {
    //左侧搜索开始
    function sousuo(parent) {
        let sousuo=document.querySelector('.search-left');
        let sousuotab=document.querySelector('.search-left-box');
        let taiyuan=parent.querySelector('.search-left-left>span');
        document.body.onclick=function (e) {
            var currobj=e.target||e.srcElement;
            if(currobj.className=="search-left-left"){
                sousuotab.style.display="block";
            }else if(currobj.innerHTML=='太原'){
                sousuotab.style.display="block";
            }
            else{
                sousuotab.style.display="none";
            }
        }
    }
    sousuo(document.querySelector('.search-box'));

    //右侧搜索开始
    function rightSearch(parent) {
       let button=parent.querySelector('.sousuo-content');

       button.onfocus=function () {
          button.placeholder="";
          button.style.boxShadow="0 0 4px blue";
          button.style.border="none";
       }
       button.onblur=function () {
           let value=button.getAttribute("value");
           if(value==""){
               button.placeholder="包月流量包";
           }
           button.style.boxShadow="none";
           button.style.border="2px solid #dfdfdf";
       }
    }
    rightSearch(document.querySelector('.search-right'));
    //nav开始
    let navli=document.querySelectorAll('.nav-content >li');
    let box=document.querySelectorAll('ul.nav-content-child');
    for(let i=1;i<navli.length;i++){
        navli[i].onmouseover=function () {
            box[i-1].style.display='block';
        }
        navli[i].onmouseout=function () {
            box[i-1].style.display='none';
        }
    }
    //banner开始
    function  banner(parent) {
        let imgs=parent.querySelectorAll('.banner-center-img');//获取图片的集合
        let bigbox=parent.querySelector('.banner-center');
        let width=bigbox.offsetWidth;
        let circleli=parent.querySelectorAll('.circle>li');
        let left=parent.querySelector('.banner-tab-left');
        let right=parent.querySelector('.banner-tab-right');
        let now=0;
        let next=0;
        let flag=true;
        let t=setInterval(move,2000);
        function move(type="r") {
            if(flag){
                flag=false;
                if(type=='r'){
                    next=now+1;
                    if(next>=imgs.length){
                        next=0;
                    }
                    imgs[next].style.left='100%';
                    animate(imgs[now],{left:-width},800);

                }else if(type=='l'){
                    next=now-1;
                    if(next<0){
                        next=imgs.length-1;
                    }
                    imgs[next].style.left='-100%';
                    animate(imgs[now],{left:width},800);
                }
                animate(imgs[next],{left:0},800,function () {
                    flag=true;
                });
                circleli[now].classList.remove('active');
                circleli[next].classList.add('active');
                now=next;
            }
        }
        bigbox.onmouseover=function () {
            clearInterval(t);
        }
        bigbox.onmouseout=function () {
            t=setInterval(move,2000);
        }
        left.onclick=function () {
            move("l");
        }
        right.onclick=function () {
            move("r");
        }
        circleli.forEach(function (value,index) {
            value.onclick=function () {
                if(!flag){
                    return;
                }
                flag=false;
                if(index>now){
                    imgs[index].style.left='100%';
                    animate(imgs[now],{left:-width},800);
                }else if(index<now){
                    imgs[index].style.left='-100%';
                    animate(imgs[now],{left:width},800);
                }
                animate(imgs[index],{left:0},800,function () {
                    flag=true;
                });
                circleli[now].classList.remove('active');
                circleli[index].classList.add('active');
                now=index;
            }
        })
    }
    banner(document.querySelector('.banner'));

   //优惠促销开始
    function yhcx(parent) {
        let bigbox=parent.querySelector('.yhcx-imgbox');
        let boxs=parent.querySelectorAll('.imgbox');
        let aside_left=parent.querySelector('.yhcx-left');
        let aside_right=parent.querySelector('.yhcx-right');
        let now=0;
        let width=parseInt(getComputedStyle(boxs[0],null).width);
        let first=bigbox.firstElementChild;
        let t=setInterval(move,2000);
        let flag=true;
        function move() {
            if(flag){
                flag=false;
                animate(bigbox,{left:-width-12},function () {
                    let first=bigbox.firstElementChild;
                    bigbox.appendChild(first);
                    bigbox.style.left=0;
                    flag=true;
                })
            }
        }
        bigbox.onmouseover=function () {
            clearInterval(t);
        }
        bigbox.onmouseout=function () {
            t=setInterval(move,3000);
        }
        aside_left.onclick=function(){
            let first=bigbox.firstElementChild;
            let last=bigbox.lastElementChild;
            bigbox.insertBefore(last,first);
            bigbox.style.left=-width-12+'px';
            animate(bigbox,{left:0})
        }
        aside_right.onclick=move;
        move();
    }
    yhcx(document.querySelector('.yhcx-box'));

   //手机移动开始
    let imgli=document.querySelectorAll('.phone-content-img img');
    let imgbox=document.querySelectorAll('.phone-content-img');
    imgbox.forEach(function (value,index) {
        value.onmouseover=function () {
            imgli[index].style.right=20+"px";
        }
        value.onmouseout=function () {
            imgli[index].style.right=0;
        }
    })

    // 公告开始
    function gongao(parent) {
        let qiant_left=parent.querySelector(".qiant-left");
        let qiant_right=parent.querySelector(".qiant-right");
        let box_ul=parent.querySelectorAll('.gongao-text>ul');
        let now=0;
        let next=0;
        let t=setInterval(move,2000);
        function move() {
            next=now+1;
            if(next==box_ul.length){
                next=0;
            }
            box_ul[now].classList.remove('active');
            box_ul[next].classList.add('active');
            now=next;
        }
        qiant_left.onclick=move;
        qiant_right.onclick=function () {
            next=now+1;
            if(next==box_ul.length){
                next=0;
            }
            box_ul[now].classList.remove('active');
            box_ul[next].classList.add('active');
            now=next;
        }

    }
    gongao(document.querySelector('.gongao-box'));

    //侧栏开始
    let zixun=document.querySelector('.zixun');
    let ques=document.querySelector('.ques');
    let jianyi=document.querySelector('.jianyi');
    zixun.onmouseover=function () {
        animate(zixun,{left:-84},300);
    }
    zixun.onmouseout=function () {
        animate(zixun,{left:-20},300);
    }
    ques.onmouseover=function () {
        animate(ques,{left:-84},300);
    }
    ques.onmouseout=function () {
        animate(ques,{left:-20},300);
    }
    jianyi.onmouseover=function () {
        animate(jianyi,{left:-84},300);
    }
    jianyi.onmouseout=function () {
        animate(jianyi,{left:-20},300);
    }


}

